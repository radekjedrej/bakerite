	<div class="section-delivery-stock">
		<div class="container delivery-stock-container">
			<div class="navigation-horizontal-line"></div>
			<div class="row">
				<div class="col-lg-6 pr-lg-0 ws-point-1">
					<div class="delivery-text-box">
						<div class="delivery-text-box-inner">
							<?php echo the_field('delivery_text_box'); ?>
						</div>	
					</div>
				</div>
				<div class="col-lg-6 pl-lg-0 delivery-image-box ws-point-2">
					<img src="<?php echo the_field('delivery_image_box'); ?>" alt="Bakerite Image">
				</div>
			</div>
		</div>
	</div>