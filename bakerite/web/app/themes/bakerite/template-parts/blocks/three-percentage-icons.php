<?php
$boxes = apply_filters('percentage_icons', get_field('percentage_icons'));
if(empty($boxes)) return;
?>  

<?php if(!empty($boxes)){?>
<section class="icons-percentage-section">
	<div class="container icons-percentage-container">
		<div class="row icons-percentage-row">
			<?php foreach($boxes as $box): ?>
			<div class="col-4 icons-percentage-wrapper text-center">
				<div class="icons-percentage-image">
					<img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-percentage.svg" alt="circle">
					<div class="icons-percentage-percentage">
						<?php echo $box['percent']; ?>
					</div>
				</div>
				<div class="icons-percentage-text text-uppercase">
						<?php echo $box['title']; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
	<?php } ?>