	<?php
	$boxes = apply_filters('three_contact_icons', get_field('three_contact_icons'));
	if(empty($boxes)) return;
	?>  

	<?php if(!empty($boxes)){?>
	<section class="section-three-icons">
		<div class="container three-icons-section">
			<div class="row">
				<?php foreach($boxes as $box): ?>
				<div class="col-sm-4 three-icons-each">
					<div class="row three-icons-row">
						<div class="col-lg-3">
							<img src="<?php echo $box['icon']; ?>" alt="Bakerite Contact">
						</div>
						<div class="col-lg-9 pl-lg-0">
							<?php echo $box['text']; ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<?php } ?>