<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

get_header();
?>
	


<section class="product-development">

	<?php get_template_part( 'template-parts/blocks/medium-banner-c'); ?>
	<!-- Extra class for less padding -->
	<div class="main-text-product-development">
		<?php get_template_part( 'template-parts/blocks/main-text'); ?>
	</div>

	<?php get_template_part( 'template-parts/blocks/product-showcase'); ?>
	<?php get_template_part( 'template-parts/blocks/products-carousel'); ?>
	<?php get_template_part( 'template-parts/blocks/contact'); ?>

</section>



<?php

get_footer();
