	<!-- Medium Banner With Text Carousel Start -->
	<?php
	$slides = apply_filters('medium_banner_c_text', get_field('medium_banner_c_text'));
	if(empty($slides)) return;
	?>  

	<div class="container-fluid medium-banner-c-container px-0">
		<div class="row">
			<div class="col-md-6 medium-banner-left pr-md-0">
				<img class="img-fluid bg-image-left" src="<?php echo  the_field('medium_banner_c_image'); ?>" alt="Bakerite banner image">
			</div>
			<div class="col-md-6 medium-banner-right">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12 pl-0">
							<?php if(!empty($slides)){?>
								<div class="owl-carousel-banner-three">
									<?php foreach($slides as $slide): ?>
										<div class="slide">
											<?php echo $slide['text']; ?>
										</div>
									<?php endforeach; ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Medium Banner With Text Carousel End -->