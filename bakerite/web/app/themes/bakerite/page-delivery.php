<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

get_header();
?>
	


<section class="bakerite-delivery">


	<?php get_template_part( 'template-parts/blocks/medium-banner'); ?>
	<?php get_template_part( 'template-parts/blocks/delivery-stock'); ?>

	<div class="container">
		<div class="dotted-border">
			<img src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/dotted-border.svg" alt="">
		</div>
	</div>

	<?php get_template_part( 'template-parts/blocks/three-icons'); ?>
	<?php get_template_part( 'template-parts/blocks/contact'); ?>


</section>



<?php

get_footer();
