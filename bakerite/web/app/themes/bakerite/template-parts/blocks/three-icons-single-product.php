	<!-- Three Icons Single Product Page Start -->
	<div class="clearfix"></div>
	<div class="single-product-three-icons single-product-three-icons">
		<section class="section-three-icons single-product-section-three-icons">
			<div class="container three-icons-section">
				<div class="row">
					
					<div class="col-sm-4 three-icons-each">
						<div class="row three-icons-row">
							<div class="col-lg-3 pr-0">
								<img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/delivery-icon-1.svg" alt="Bakerite Contact">
							</div>
							<div class="col-lg-9 pl-lg-0">
								<p>Get in touch today, Call <a class="text-nowrap" href="tel:08009998876">0800 999 8876</a> </p>
							</div>
						</div>
					</div>
					<div class="col-sm-4 three-icons-each">
						<div class="row three-icons-row">
							<div class="col-lg-3 pr-0">
								<img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/delivery-icon-4.svg" alt="Bakerite Contact">
							</div>
							<div class="col-lg-9 pl-lg-0">
								<p><a class="red-text" href="#hover_contact_single">Click here </a> to email us your enquiries</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4 three-icons-each">
						<div class="row three-icons-row">
							<div class="col-lg-3 pr-0">
								<img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/delivery-icon-2.svg" alt="Bakerite Contact">
							</div>
							<div class="col-lg-9 pl-lg-0">
								<p>Quick delivery stocked and held in the UK</a> </p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
	</div>
	<!-- Three Icons Single Product Page Start -->