	<div class="section-product-showcase">
		<div class="container product-showcase-container">
			<div class="product-showcase-container-inner">
				<div class="navigation-horizontal-line"></div>
				<div class="row product-showcase-row">
					<div class="col-lg-6 pr-lg-0 ws-point-1">
						<div class="product-showcase-text-wrapper">
							<div class="product-showcase-text">
								<?php echo the_field('product_showcase_text'); ?>
							</div>
						</div>
					</div>
					<div class="col-lg-6 pl-lg-0 ws-point-2">
						<div class="products-showcase-image-wrapper">
							<img src="<?php echo the_field('product_showcase_image'); ?>" alt="Bakerite Showcase"> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>