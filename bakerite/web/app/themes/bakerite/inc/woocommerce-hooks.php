<?php 

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 250,
        'single_image_width'    => 300,
        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 3,
            'min_columns'     => 2,
            'max_columns'     => 3,
        ),
    ));
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

// Remove Sorting Archive Category Page
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
// Add the result count from WooCommerce
add_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 90 );
// Insert percentage section
function single_product_percentage_icons(){
    echo get_template_part( 'template-parts/blocks/three-percentage-icons');
}
add_action( 'woocommerce_single_product_summary', 'single_product_percentage_icons',21 );
// Three icons single product page
function single_product_three_icons(){
    echo get_template_part( 'template-parts/blocks/three-icons-single-product');
}
add_action( 'woocommerce_after_single_product_summary', 'single_product_three_icons',21 );
// Adds product SKUs above the "Add to Cart" buttons
function skyverge_shop_display_skus() {
	global $product;
	if ( $product->get_sku() ) {
		echo '<div class="product-meta">' . $product->get_sku() . '</div>';
	}
}
add_action( 'woocommerce_shop_loop_item_title', 'skyverge_shop_display_skus', 1 );
// Rename Tabs Single Product
add_filter( 'woocommerce_product_tabs', 'woo_rename_tab', 98);
function woo_rename_tab($tabs) {
$tabs['description']['title'] = 'Product Specification';return $tabs;
}
// Remove Tab title for Description Tab
add_filter('woocommerce_product_description_heading',
'sam_product_description_heading');

function sam_product_description_heading() {
    return '';
}
// Remove zoom effect from single product main image
function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'wp', 'remove_image_zoom_support', 100 );
// SKU product_meta moved under title
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 6 );
// Change SKU To Product Code 
function translate_woocommerce($translation, $text, $domain) {
    if ($domain == 'woocommerce') {
        switch ($text) {
            case 'SKU':
                $translation = 'Key code: ';
                break;
            case 'SKU:':
                $translation = 'Key code: ';
                break;
        }
    }
    return $translation;
}
add_filter('gettext', 'translate_woocommerce', 10, 3);

