  
  <section class="view-all-posts">
    <div class="container view-all-posts-container">
      <div class="view-all-posts-inner">
        <?php
          wp_nav_menu( array(
          'theme_location' => 'menu-2',
          'menu_id'        => 'view-all-posts',
          'menu_class' => 'view-all-posts'
        ) );
        ?>
      </div>
    </div>
  </section>
