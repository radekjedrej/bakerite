
<section class="all-posts all-posts-videos">
  <div class="container all-posts-container">
    <div class="row all-posts-row">

    <?php
    $homepageEvents = new WP_Query(array(
      'posts_per_page' => 6,
      'post_type' => array(
        'post'
      ),
      'category_name' => 'video',
      'post_status' => 'publish',
      'orderby' => 'publish_date',
      'order' => 'ASC'
    ));
    /* Start the Loop */
    while ( $homepageEvents->have_posts() ) : $homepageEvents->the_post(); 

    $boxText = get_field('watch_video_link');
    
    ?>
  
      <div class="col-sm-6 col-lg-4 all-posts-wrapper">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="all-posts-inner">
            <div class="all-posts-title-box">
              <div class="posts-main-text">
                <div class="main-title">
                  <?php the_title(); ?>
                </div>
                <a href="<?php the_permalink(); ?>">
                  <div class="post-type-title">
                    <?php if(in_category('article')) { ?>
                      Read Article
                    <?php } elseif(in_category('video')) { ?>
                      Watch Video
                    <?php } else { ?>
                      SEE MORE
                    <?php } ?>
                  </div>
                </a>
                <div class="post-date-added">
                  <?php the_time('d M, Y'); ?>
                </div>                
              </div>
              <div class="post-image">
                  <?php 
                  if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  ?>
                    <div class="tile hover-target">
                      <img class="img-fluid hover-scale" src="<?php the_post_thumbnail_url(); ?>" alt="Bakerite Post">                     
                      <a href="<?php the_permalink(); ?>">
                        <div class="sheet hover-display">
                          <div class="text-center">
                            <div class="post-type-title">
                              <?php if(in_category('article')) { ?>
                                Read Article
                              <?php } elseif(in_category('video')) { ?>
                                Watch Video
                              <?php } else { ?>
                                SEE MORE
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>               
                  <?php
                  } else {
                  ?>
                    <div class="tile hover-target">          
                      <img class="img-fluid hover-scale" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/b-box-6-a.jpg" alt="Bakerite Post">                    
                      <a href="<?php the_permalink(); ?>">
                        <div class="sheet hover-display">
                          <div class="text-center">
                            <div class="post-type-title">
                              <?php if(in_category('article')) { ?>
                                Read Article
                              <?php } elseif(in_category('video')) { ?>
                                Watch Video
                              <?php } else { ?>
                                SEE MORE
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>                     
                  <?php
                  }
                  ?>
                  <img class="posts-icon" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/box-icon.svg" alt="Icon">
              </div>
            </div>
          </div>
        </article><!-- #post-<?php the_ID(); ?> -->
      </div>
    
    <?php
    endwhile;
    // Restore original post data.
    wp_reset_postdata();
    ?>

    </div> <!-- End of Row -->
  </div> <!-- End of Container -->

  <div class="container load-more-posts-container">
    <div class="row">
      <div class="posts-button">
        <?php echo do_shortcode('[ajax_load_more preloaded="true" posts_per_page="6" preloaded_amount="6" offset="6" repeater="template_13" post_type="post" category="video" pause="true" button_label="Load More Videos" css_classes="posts-object-questions"]'); ?>
      </div>
    </div>
  </div>
  
</section>