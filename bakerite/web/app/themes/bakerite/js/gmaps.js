function initMap() {

  function buildMap() {
    // Create a map object and specify the DOM element
    // for display.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 11
    });

    var contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">' +
      '<a href="https://goo.gl/maps/dsChPkjG91v">' +
      'BakeRite</a></h1>' +
      '</div>' +
      '</div>';


    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    // Create a marker and set its position.
    var marker = new google.maps.Marker({
      map: map,
      position: markerLatLng,
      title: 'The BakeRite Company'
    });
    marker.addListener('click', function () {
      infowindow.open(map, marker);
    });
  }

  if (window.matchMedia("(max-width: 893px)").matches) {
    var myLatLng = { lat: 53.7497123, lng: -0.535374 };
    var markerLatLng = { lat: 53.7217145, lng: -0.5348031 };

    buildMap();

  } else {
    var myLatLng = { lat: 53.7497123, lng: -0.275374 };
    var markerLatLng = { lat: 53.7217145, lng: -0.5348031 };

    buildMap();
  }
}
