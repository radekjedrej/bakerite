<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container single-post-container">
		<div class="single-post-row row">
			<div class="col-lg-9 pr-lg-5 single-post-main-text">
				<div class="entry-content">

					<!-- Title Start  -->
					<?php
						if ( is_singular() ) :
							the_title( '<h1 class="entry-title single-post-heading">', '</h1>' );
						else :
							the_title( '<h2 class="entry-title single-post-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;
					?>
					<!-- Title End  -->
					
					<!-- Post Date + Post Subcategory Start -->
					<div class="single-post-info-wrapper">
						<?php $pfx_date = get_the_date( 'd M, Y' ); ?>
							<div class="single-post-date-wrapper">
								<span><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-calendar.svg" alt="Calendar Icon"></span>
								<span class="single-post-date"><?php echo $pfx_date; ?></span>
							</div>

						<?php $categories = get_the_category();	?>
						<?php if(!empty($categories) && (!empty($categories[1]->cat_name))){ ?>
							<div class="single-post-category-wrapper">
								<span><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-category.svg" alt="Category Icon"></span>
								<span class="single-post-category">Category: <?php echo $categories[1]->cat_name; ?></span>
							</div>
						<?php } ?>
					</div>
					<!-- Post Date + Post Subcategory End -->
					
					<!-- Main Content Start -->
					<div class="main-entry-wrapper">
						<?php	the_content();	?>
					</div>
					<!-- Main Content End -->
					
					<!-- Share Icons Start -->
					<div class="share-bakerite">
						<div class="container">
							<div class="row share-bakerite-row">
								<div class="col-md-7 share-bakerite-title">
									<h3>Share THE BAKERITE Article TODAY!</h3>
								</div>
								<div class="col-md-5 share-bakerite-icons">
									<a href="<?php the_field('share_twitter', 'option'); ?>"><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-twitter.svg" alt="Share post Twitter"></a>
									<a href="<?php the_field('share_facebook', 'option'); ?>"><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-facebook.svg" alt="Share post Facebook"></a>
									<a href="<?php the_field('share_linkedin', 'option'); ?>"><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-linkedin.svg" alt="Share post Linkedin"></a>
									<a href="<?php the_field('share_pinterest', 'option'); ?>"><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/icon-pinerest.svg" alt="Share post Pinerest"></a>
								</div>
							</div>
						</div>
					</div>
					<!-- Share Icons End -->

					<!-- GoBack Button Start -->
					<div class="single-post-back-button">
						<div class="container back-button-container text-center">
							<a href="<?php echo home_url(); ?>/view-all-posts/">BACK TO ARTICLES</a>
						</div>
					</div>
					<!-- GoBack Button End -->
					
				</div><!-- .entry-content -->
			</div>
			<div class="col-lg-3 single-post-widget">

				<?php 			
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Categories Posts') ) : ?>
					<ul>
							<?php wp_get_archives('title_li=&type=monthly'); ?>
					</ul>
					<ul>
							<?php wp_list_categories('show_count=0&title_li='); ?>
					</ul>
				<?php endif; ?> 

			</div>
		</div>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
