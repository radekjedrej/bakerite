(function ($) {

// Front Page Categories
$('.js--wp-11').waypoint(function (direction) {
  $('.js--wp-11').addClass('animated fadeIn');
}, {
    offset: '70%'
});

// Product Development
  $('.product-showcase-row').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page
$('.delivery-stock-container').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page 2
$('.delivery-point-1').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page 2
$('.delivery-point-2').waypoint(function (direction) {
  $('.ws-point-3').addClass('animated fadeInLeft');
  $('.ws-point-4').addClass('animated fadeInRight');
}, {
  offset: '70%'
});

// Image 1 Assurance
$('.img-anim-1').waypoint(function (direction) {
  $('.img-anim-1').addClass('animated fadeIn');
}, {
  offset: '70%'
});

  // Image 2 Assurance
$('.img-anim-2').waypoint(function (direction) {
  $('.img-anim-2').addClass('animated fadeIn');
}, {
  offset: '70%'
});

  // Image 3 Assurance
$('.img-anim-3').waypoint(function (direction) {
  $('.img-anim-3').addClass('animated fadeIn');
}, {
  offset: '70%'
});

// Image 4 Assurance
$('.img-anim-4').waypoint(function (direction) {
  $('.img-anim-4').addClass('animated fadeIn');
}, {
  offset: '70%'
});
  
// Image 5 Assurance
$('.img-anim-5').waypoint(function (direction) {
  $('.img-anim-5').addClass('animated fadeIn');
}, {
  offset: '70%'
});

})(jQuery);