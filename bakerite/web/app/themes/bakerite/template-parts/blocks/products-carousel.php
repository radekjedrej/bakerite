	<?php
			$prod_categories = get_terms( 'product_cat', array(
					'orderby'    => 'name',
					'order'      => 'DESC',
					'hide_empty' => 1,
					'parent' => 0
			));   
	?>

	<div class="section-products-carousel">
		<div class="container products-carousel-container">
			<div class="products-carousel-heading">
				<h2>Explore our product ranges</h2>
			</div>
			<div class="row products-carousel-row px-3">
				<div class="owl-carousel-products"> <!-- Owl Carousel Start -->
					<?php $i = 1;  ?>
					<?php foreach( $prod_categories as $prod_cat ) :
							
						$cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
						$cat_thumb_url = wp_get_attachment_url( $cat_thumb_id );
						$term_link = get_term_link( $prod_cat, 'product_cat' );
						?>
							<div class="slide">
								<div class="category-wrapper">
										<div class="category-box">
												<a href="<?php echo $term_link; ?>">  
													<div class="category-box-heading-text">
															<h3 class="card-title text-uppercase heading-<?php echo $i; ?>"><?php echo $prod_cat->name; ?></h3>
													</div>
												</a>
												<a href="<?php echo $term_link; ?>"><img class="img-fluid" src="<?php echo $cat_thumb_url; ?>" alt="<?php echo $prod_cat->name; ?>" /></a>
										</div>
								</div>
							</div>
							
							<?php $i++; ?>
					<?php endforeach; wp_reset_query(); ?>  	
				</div>	<!-- Owl Carousel End -->
			</div>
		</div>
	</div>