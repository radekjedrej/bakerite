(function ($) {

  var menuCategories = $('#yith-woo-ajax-navigation-3');
  asideMenuButton = menuCategories.find('h2');
  asideMenuContent = menuCategories.find('ul');

  asideMenuButton.on('click', function (e) {
    var windowWidth = $(window).width();
    if (windowWidth < 992) {
      e.preventDefault();

      asideMenuContent.slideToggle(200, function () {
        asideMenuButton.toggleClass('hide');
      });
    }
  });

  var menuCategories2 = $('#yith-woo-ajax-navigation-4');
  var asideMenuButton2 = menuCategories2.find('h2');
  asideMenuContent2 = menuCategories2.find('ul');


  asideMenuButton2.on('click', function (e) {
    var windowWidth = $(window).width();
    if (windowWidth < 992) {
      e.preventDefault();

      asideMenuContent2.slideToggle(100, function () {
        asideMenuButton2.toggleClass('hide');
      });
    }
  });

})(jQuery);



