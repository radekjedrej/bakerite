<!-- Contact Options Section Start -->
<?php
$boxes = apply_filters('about_us_promise', get_field('about_us_promise'));
if(empty($boxes)) return;
$i = 1;
?> 
  
<?php if(!empty($boxes)){?>  
  <div class="section-about-us-promise">
    <div class="arrow-top"></div>
    <div class="container about-us-promise-container">
      <div class="row about-us-promise-row">
        <?php foreach($boxes as $box): ?>
        <div class="col-6 col-md-4 about-us-promise-wrapper about-us-promise-each-<?php echo $i; ?>">
          <div class="about-us-promise-wrapper-inner">
            <div class="about-us-image-wrapper">
              <img src="<?php echo $box['icon']; ?>" alt="About Us Icon">
            </div>
            <div class="about-us-text-wrapper">
              <?php echo $box['text']; ?>
            </div>
          </div>
        </div>
        <?php $i++; ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
<?php } ?>