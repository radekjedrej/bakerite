<section class="home-page-text">
  <div class="container">
    <?php echo the_content(); ?>

    <div class="dotted-border">
      <img src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/dotted-border.svg" alt="">
    </div>
  </div>
</section>