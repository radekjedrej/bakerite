<?php
    $prod_categories = get_terms( 'product_cat', array(
        'orderby'    => 'name',
        'order'      => 'DESC',
        'hide_empty' => 1,
        'number' => 9,
        'parent' => 0
    ));   
?>

<section class="front-page-categories">
    <div class="container">
        <div class="row front-category-row pt-0 pb-lg-1">
            <?php $i = 1;  ?>
            <?php foreach( $prod_categories as $prod_cat ) :
                
                $cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
                $cat_thumb_url = wp_get_attachment_url( $cat_thumb_id );
                $term_link = get_term_link( $prod_cat, 'product_cat' );
                
                ?>
                <div class="category-wrapper col-sm-6 col-md-4 js--wp-11 delay-05s">
                    <div class="category-box">
                        <a href="<?php echo $term_link; ?>">  
                          <div class="category-box-heading-text">
                              <h3 class="card-title text-uppercase heading-<?php echo $i; ?>"><?php echo $prod_cat->name; ?></h3>
                          </div>
                        </a>
                        <a href="<?php echo $term_link; ?>"><img class="img-fluid" src="<?php echo $cat_thumb_url; ?>" alt="<?php echo $prod_cat->name; ?>" /></a>
                    </div>
                </div>
                
                <?php $i++; ?>
            <?php endforeach; wp_reset_query(); ?>       
        </div>

        <div class="border-bottom">
            
        </div>

    </div>
</section>