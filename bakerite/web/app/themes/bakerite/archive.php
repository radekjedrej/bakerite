<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

get_header();
?>

	<div id="primary" class="content-area posts-search-template">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<div class="container">
					<?php
					the_archive_title( '<h1 class="page-title posts-search-heading">', '</h1>' );
					the_archive_description( '<div class="archive-description posts-search-heading">', '</div>' );
					?>
				</div>
			</header><!-- .page-header -->

			<section class="all-posts">
				<div class="container all-posts-container">
					<div class="row all-posts-row">
						
							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();
								
								/*
								* Include the Post-Type-specific template for the content.
								* If you want to override this in a child theme, then include a file
								* called content-___.php (where ___ is the Post Type name) and that will be used instead.
								*/
								get_template_part( 'template-parts/content-search-categories', get_post_type() );

								?> <?php
							endwhile;


						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
					</div>
				</div>

				<?php $cat = get_query_var('cat');

				$category = get_category ($cat);	?>
		
				<div class="container load-more-posts-container">
					<div class="row">
						<div class="posts-button">
							<?php echo do_shortcode('[ajax_load_more preloaded="true" posts_per_page="6" preloaded_amount="6" offset="6" repeater="template_13" post_type="post" category="'. $category->slug .'" pause="true" button_label="Load More Articles" css_classes="posts-object-questions"]'); ?>
						</div>      
					</div>
				</div>
				
			</section>

			<div class="entry-content contact-main-section">
				<div class="arrow-top"></div>
				<div class="container container-contact contact">
					<div class="contact-heading text-center">
						<h1>Complete our quick response form and we’ll get back to you</h1>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
