<section class="three-boxes">
  <div class="container">

    <?php
    $threeBoxes = apply_filters('three_boxes', get_field('three_boxes'));
    if(empty($threeBoxes)) return;
    $i = 1;
    ?>

    <div class="row px-3 px-lg-0">
      <?php foreach($threeBoxes as $threeBoxe): ?>  
      <div class="col-md-4 px-0 the-box-single box-single-<?php echo $i; ?>">
          <div class="the-box-single-wrapper">
            <a href="<?php echo $threeBoxe['link']; ?>">
              <div class="box-wrapper">
                <div class="box-title text-uppercase ">
                  <?php echo $threeBoxe['text']; ?>                        
                </div>
                <div class="box-image">
                  <img class="img-fluid" src="<?php echo $threeBoxe['image']; ?>" alt="Bakerite Image">  
                  <div class="box-icon">
                      <img src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/box-icon.svg" alt="Icon">
                  </div>   
                </div>
              </div>
            </a>
        </div>
      </div>
      <?php $i++; ?>
      <?php endforeach; ?>
    </div>


  </div>
</section>