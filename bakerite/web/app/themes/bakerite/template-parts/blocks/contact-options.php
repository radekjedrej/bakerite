	<!-- Contact Options Section Start -->
	<?php
	$boxes = apply_filters('contact_us_options', get_field('contact_us_options'));
	if(empty($boxes)) return;
	?>  

	<?php if(!empty($boxes)){?>
	<div class="contact-options">
		<div class="container contact-options-container">
			<div class="row">
				<?php foreach($boxes as $box): ?>
				<div class="col-sm-6 contact-options-each mb-4 mb-lg-0">
					<div class="row contact-options-row text-center text-lg-left">
						<div class="col-lg-6 pl-lg-1 contact-options-image">
							<img src="<?php echo $box['image']; ?>" alt="Bakerite Contact">
						</div>
						<div class="col-lg-6 pl-lg-0 contact-options-text">
							<?php echo $box['text']; ?>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- Contact Options Section End -->