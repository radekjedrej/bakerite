(function ($) {

// Front Page Categories
$('.js--wp-11').waypoint(function (direction) {
  $('.js--wp-11').addClass('animated fadeIn');
}, {
    offset: '70%'
});

// Product Development
  $('.product-showcase-row').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page
$('.delivery-stock-container').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page 2
$('.delivery-point-1').waypoint(function (direction) {
  $('.ws-point-1').addClass('animated fadeInLeft');
  $('.ws-point-2').addClass('animated fadeInRight');
}, {
  offset: '70%'
});
// Delivery Page 2
$('.delivery-point-2').waypoint(function (direction) {
  $('.ws-point-3').addClass('animated fadeInLeft');
  $('.ws-point-4').addClass('animated fadeInRight');
}, {
  offset: '70%'
});

// Image 1 Assurance
$('.img-anim-1').waypoint(function (direction) {
  $('.img-anim-1').addClass('animated fadeIn');
}, {
  offset: '70%'
});

  // Image 2 Assurance
$('.img-anim-2').waypoint(function (direction) {
  $('.img-anim-2').addClass('animated fadeIn');
}, {
  offset: '70%'
});

  // Image 3 Assurance
$('.img-anim-3').waypoint(function (direction) {
  $('.img-anim-3').addClass('animated fadeIn');
}, {
  offset: '70%'
});

// Image 4 Assurance
$('.img-anim-4').waypoint(function (direction) {
  $('.img-anim-4').addClass('animated fadeIn');
}, {
  offset: '70%'
});
  
// Image 5 Assurance
$('.img-anim-5').waypoint(function (direction) {
  $('.img-anim-5').addClass('animated fadeIn');
}, {
  offset: '70%'
});

})(jQuery);
// ##############
// Carousels
// ##############

//About Us Carousel
(function ($) {
  $('.owl-carousel-home-page-two').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    items: 2,
    nav: false,
    dots: true,
    margin: 0,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      }
    }
  });
})(jQuery);

//Products carousel three
(function ($) {
  $('.owl-carousel-products-three').owlCarousel({
    loop: true,
    autoplay: false,
    autoplayHoverPause: true,
    items: 1,
    nav: false,
    dots: true,
    margin: 0,
  });
})(jQuery);

//Medium Banner Text Carousel 3
(function ($) {
  $('.owl-carousel-banner-three').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    nav: false,
    dots: true,
    items: 1
  });
})(jQuery);

//Products Carousel
(function ($) {
  $('.owl-carousel-products').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    nav: false,
    margin: 0,
    responsive: {
      0: {
        items: 1,
        nav: false
      },
      440: {
        items: 2,
        nav: false
      },
      768: {
        items: 3,
        nav: true
      }
    }
  });
})(jQuery);



// Animate - Toggle == Menu Burger   
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD module
    define(factory);
  } else if (typeof exports === 'object') {
    // CommonJS-like environment (i.e. Node)
    module.exports = factory();
  } else {
    // Browser global
    root.transformicons = factory();
  }
}(this || window, function () {

  // ####################
  // MODULE TRANSFORMICON
  // ####################
  'use strict';

  var
    tcon = {}, // static class
    _transformClass = 'tcon-transform',

    // const
    DEFAULT_EVENTS = {
      transform: ['click'],
      revert: ['click']
    };

  // ##############
  // private methods
  // ##############

  /**
   * Normalize a selector string, a single DOM element or an array of elements into an array of DOM elements.
   * @private
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements
   * @returns {array} Array of DOM elements
   */
  var getElementList = function (elements) {
    if (typeof elements === 'string') {
      return Array.prototype.slice.call(document.querySelectorAll(elements));
    } else if (typeof elements === 'undefined' || elements instanceof Array) {
      return elements;
    } else {
      return [elements];
    }
  };

  /**
   * Normalize a string with eventnames separated by spaces or an array of eventnames into an array of eventnames.
   * @private
   *
   * @param {(string|array)} elements - String with eventnames separated by spaces or array of eventnames
   * @returns {array} Array of eventnames
   */
  var getEventList = function (events) {
    if (typeof events === 'string') {
      return events.toLowerCase().split(' ');
    } else {
      return events;
    }
  };

  /**
   * Attach or remove transformicon events to one or more elements.
   * @private
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
   * @param {object} [events] - An Object containing one or more special event definitions
   * @param {boolean} [remove=false] - Defines wether the listeners should be added (default) or removed.
   */
  var setListeners = function (elements, events, remove) {
    var
      method = (remove ? 'remove' : 'add') + 'EventListener',
      elementList = getElementList(elements),
      currentElement = elementList.length,
      eventLists = {};

    // get events or use defaults
    for (var prop in DEFAULT_EVENTS) {
      eventLists[prop] = (events && events[prop]) ? getEventList(events[prop]) : DEFAULT_EVENTS[prop];
    }

    // add or remove all events for all occasions to all elements
    while (currentElement--) {
      for (var occasion in eventLists) {
        var currentEvent = eventLists[occasion].length;
        while (currentEvent--) {
          elementList[currentElement][method](eventLists[occasion][currentEvent], handleEvent);
        }
      }
    }
  };

  /**
   * Event handler for transform events.
   * @private
   *
   * @param {object} event - event object
   */
  var handleEvent = function (event) {
    tcon.toggle(event.currentTarget);
  };

  // ##############
  // public methods
  // ##############

  /**
   * Add transformicon behavior to one or more elements.
   * @public
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
   * @param {object} [events] - An Object containing one or more special event definitions
   * @param {(string|array)} [events.transform] - One or more events that trigger the transform. Can be an Array or string with events seperated by space.
   * @param {(string|array)} [events.revert] - One or more events that trigger the reversion. Can be an Array or string with events seperated by space.
   * @returns {transformicon} transformicon instance for chaining
   */
  tcon.add = function (elements, events) {
    setListeners(elements, events);
    return tcon;
  };

  /**
   * Remove transformicon behavior from one or more elements.
   * @public
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
   * @param {object} [events] - An Object containing one or more special event definitions
   * @param {(string|array)} [events.transform] - One or more events that trigger the transform. Can be an Array or string with events seperated by space.
   * @param {(string|array)} [events.revert] - One or more events that trigger the reversion. Can be an Array or string with events seperated by space.
   * @returns {transformicon} transformicon instance for chaining
   */
  tcon.remove = function (elements, events) {
    setListeners(elements, events, true);
    return tcon;
  };

  /**
   * Put one or more elements in the transformed state.
   * @public
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be transformed
   * @returns {transformicon} transformicon instance for chaining
   */
  tcon.transform = function (elements) {
    getElementList(elements).forEach(function (element) {
      element.classList.add(_transformClass);
    });
    return tcon;
  };

  /**
   * Revert one or more elements to the original state.
   * @public
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be reverted
   * @returns {transformicon} transformicon instance for chaining
   */
  tcon.revert = function (elements) {
    getElementList(elements).forEach(function (element) {
      element.classList.remove(_transformClass);
    });
    return tcon;
  };

  /**
   * Toggles one or more elements between transformed and original state.
   * @public
   *
   * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
   * @returns {transformicon} transformicon instance for chaining
   */
  tcon.toggle = function (elements) {
    getElementList(elements).forEach(function (element) {
      tcon[element.classList.contains(_transformClass) ? 'revert' : 'transform'](element);
    });
    return tcon;
  };

  return tcon;
}));

transformicons.add('.tcon');








(function ($) {

  var menuCategories = $('#yith-woo-ajax-navigation-3');
  asideMenuButton = menuCategories.find('h2');
  asideMenuContent = menuCategories.find('ul');

  asideMenuButton.on('click', function (e) {
    var windowWidth = $(window).width();
    if (windowWidth < 992) {
      e.preventDefault();

      asideMenuContent.slideToggle(200, function () {
        asideMenuButton.toggleClass('hide');
      });
    }
  });

  var menuCategories2 = $('#yith-woo-ajax-navigation-4');
  var asideMenuButton2 = menuCategories2.find('h2');
  asideMenuContent2 = menuCategories2.find('ul');


  asideMenuButton2.on('click', function (e) {
    var windowWidth = $(window).width();
    if (windowWidth < 992) {
      e.preventDefault();

      asideMenuContent2.slideToggle(100, function () {
        asideMenuButton2.toggleClass('hide');
      });
    }
  });

})(jQuery);



