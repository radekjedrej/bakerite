<?php
$artworks = apply_filters('h_page_carousel_three', get_field('h_page_carousel_three'));
if(empty($artworks)) return;
?>

<section class="home-page-carousel-two">
  <div class="home-page-carousel-two-extra">
    <div class="container">
      <div class="row px-3">
          <div class="owl-carousel-products-three">

            <?php foreach($artworks as $artwork): ?>

              <div class="slide">
                <div class="main-carousel-wrapper">
                  
                  <div class="col-md-6 pr-md-0">
                    <div class="main-carousel-text-wrapper">
                      <?php echo $artwork['text']; ?>
                    </div>                
                  </div>

                  <div class="col-md-6 pl-md-0 main-carousel-image-wrapper">
                    <img class="img-fluid" src="<?php echo $artwork['image']; ?>" alt="Bakerite product image">
                  </div>

                </div>             
              </div>

            <?php endforeach; ?>

          </div>
      </div>
    </div>
  </div>
</section>