  
  <div class="entry-content contact-main-section">
    <div class="arrow-top"></div>
    <div class="container container-contact contact">
      <div class="contact-heading text-center">
        <h1>Complete our quick response form and we’ll get back to you</h1>
      </div>
      <?php echo the_field('contact_us');	?>
    </div>
	</div><!-- .entry-content -->