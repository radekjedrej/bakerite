// ##############
// Carousels
// ##############

//About Us Carousel
(function ($) {
  $('.owl-carousel-home-page-two').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    items: 2,
    nav: false,
    dots: true,
    margin: 0,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      }
    }
  });
})(jQuery);

//Products carousel three
(function ($) {
  $('.owl-carousel-products-three').owlCarousel({
    loop: true,
    autoplay: false,
    autoplayHoverPause: true,
    items: 1,
    nav: false,
    dots: true,
    margin: 0,
  });
})(jQuery);

//Medium Banner Text Carousel 3
(function ($) {
  $('.owl-carousel-banner-three').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    nav: false,
    dots: true,
    items: 1
  });
})(jQuery);

//Products Carousel
(function ($) {
  $('.owl-carousel-products').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    nav: false,
    margin: 0,
    responsive: {
      0: {
        items: 1,
        nav: false
      },
      440: {
        items: 2,
        nav: false
      },
      768: {
        items: 3,
        nav: true
      }
    }
  });
})(jQuery);