<section id="main-banner" class="main-banner">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
      <?php
        $boxes = apply_filters('home_page_carousel_one', get_field('home_page_carousel_one'));
        if(empty($boxes)) return;
        $i = 0;
      ?>
      <?php foreach($boxes as $box): ?> 
      <div class="carousel-item carousel-image-<?php echo $i; ?> <?= $i === 0 ? 'active' : ''?>">
        <div class="container-fluid px-lg-0">

            <div class="row">
              <div class="col-lg-6 image-box px-0">
                <img src="<?php echo $box['image']; ?>" alt="">
              </div>
              <div class="col-lg-6 text-box px-0 d-flex align-items-center">
                <div class="main-text text-uppercase">

                  <div class="row">
                    <div class="offset-lg-3 col-lg-9 inner-text-wrapper">
                      <?php echo $box['text']; ?>
                      <div id="button-style">
                        <a href="<?php echo $box['button_link']; ?>" class="mybutton-outline button"><?php echo $box['button_text']; ?></a>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>

          <div class="product-image">
            <img class="img-fluid" src="<?php echo $box['product_image']; ?>" alt="Product Image">
          </div>
                
        </div>
      </div>
      <?php $i++; ?>
      <?php endforeach; ?>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="offset-lg-6 col-lg-6">
          <div class="indocator-wrapper">
            <ol class="carousel-indicators">
              <?php
                $boxes = apply_filters('home_page_carousel_one', get_field('home_page_carousel_one'));
                if(empty($boxes)) return;
                $i = 0;
              ?>
              <?php foreach($boxes as $box): ?> 
                <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?= $i === 0 ? 'active' : ''?>"></li>
              <?php $i++; ?>
              <?php endforeach; ?>
            </ol>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>		