<div class="col-sm-6 col-lg-4 all-posts-wrapper">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="all-posts-inner">
      <div class="all-posts-title-box">
        <div class="posts-main-text">
          <div class="main-title">
            <?php the_title(); ?>
          </div>
          <a href="<?php the_permalink(); ?>">
            <div class="post-type-title">
              <?php if(in_category('article')) { ?>
                Read Article
              <?php } elseif(in_category('video')) { ?>
                Watch Video
              <?php } else { ?>
                SEE MORE
              <?php } ?>
            </div>
          </a>
          <div class="post-date-added">
            <?php the_time('d M, Y'); ?>
          </div>                
        </div>
        <div class="post-image">
            <?php 
            if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
            ?>
              <div class="tile hover-target">
                <img class="img-fluid hover-scale" src="<?php the_post_thumbnail_url(); ?>" alt="Bakerite Post">                     
                <a href="<?php the_permalink(); ?>">
                  <div class="sheet hover-display">
                    <div class="text-center">
                      <div class="post-type-title">
                        <?php if(in_category('article')) { ?>
                          Read Article
                        <?php } elseif(in_category('video')) { ?>
                          Watch Video
                        <?php } else { ?>
                          SEE MORE
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </a>
              </div>               
            <?php
            } else {
            ?>
              <div class="tile hover-target">          
                <img class="img-fluid hover-scale hover-scale" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/b-box-6-a.jpg" alt="Bakerite Post">                    
                <a href="<?php the_permalink(); ?>">
                  <div class="sheet hover-display">
                    <div class="text-center">
                      <div class="post-type-title">
                        <?php if(in_category('article')) { ?>
                          Read Article
                        <?php } elseif(in_category('video')) { ?>
                          Watch Video
                        <?php } else { ?>
                          SEE MORE
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </a>
              </div>                     
            <?php
            }
            ?>
            <img class="posts-icon" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/box-icon.svg" alt="Icon">
        </div>
      </div>
    </div>
  </article><!-- #post-<?php the_ID(); ?> -->
</div>

<?php

// Restore original post data.
wp_reset_postdata();
?>

