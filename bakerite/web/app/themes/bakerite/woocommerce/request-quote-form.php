<?php
/**
 * Form to Request a quote
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @version 1.0.0
 * @author  YITH
 */
$current_user = array();
if ( is_user_logged_in() ) {
    $current_user = get_user_by( 'id', get_current_user_id() );
}

$user_name = ( ! empty( $current_user ) ) ?  $current_user->display_name : '';
$user_email = ( ! empty( $current_user ) ) ?  $current_user->user_email : '';
?>
<div class="yith-ywraq-mail-form-wrapper">
    <h3><?php _e( 'Send the request', 'yith-woocommerce-request-a-quote' ) ?></h3>

    <form id="yith-ywraq-mail-form" name="yith-ywraq-mail-form" action="<?php echo esc_url( YITH_Request_Quote()->get_raq_page_url() ) ?>" method="post">

        <div class="row">

            <div class="col-12 validate-required" id="rqa_name_row">
                <div class="form-group">
                    <input type="text" class="input-block form-control" name="rqa_name" id="rqa-name" placeholder="Name*" value="<?php echo $user_name ?>" required>
                </div>
            </div>
            <div class="col-12 validate-required" id="rqa_email_row">
                <div class="form-group">
                    <input type="email" class="input-block form-control" name="rqa_email" id="rqa-rqa_email" placeholder="Email*" value="<?php echo $user_email ?>" required>
                </div>
            </div>
            <div class="col-12 validate-required" id="rqa_company_row">
                <div class="form-group">
                    <input type="text" class="input-block form-control" name="rqa_company" id="rqa-rqa_company" placeholder="Company">
                </div>
            </div>
            <div class="col-12 validate-required" id="rqa_telephone_row">
                <div class="form-group">
                    <input type="tel" class="input-block form-control" name="rqa_telephone" id="rqa-rqa_telephone" placeholder="Telephone">
                </div>
            </div>
            <div class="col-12 validate-required" id="rqa_message_row">
                <div class="form-group">
                    <textarea name="rqa_message" class="input-block form-control" id="rqa-message" placeholder="Additional details..." rows="8"></textarea>
                </div>
            </div>
        
            <?php if( 'yes' == get_option('ywraq_add_privacy_checkbox', 'no') ): ?>
                <div class="ywraq-privacy-wrapper">
                    <p class="form-row"
                    id="rqa_privacy_description_row"><?php echo ywraq_replace_policy_page_link_placeholders( get_option( 'ywraq_privacy_description' ) ) ?></p>
                    <p class="form-row" id="rqa_privacy_row">
                        <input type="checkbox" name="rqa_privacy" id="rqa_privacy" required>
                        <label for="rqa_privacy"
                            class=""><?php echo ywraq_replace_policy_page_link_placeholders(get_option('ywraq_privacy_label') ) ?>
                            <abbr class="required" title="required">*</abbr>
                        </label>
                    </p>
                </div>
            <?php endif ?>

            <div class="col-12">
                <p class="form-row">
                    <input type="hidden" id="raq-mail-wpnonce" name="raq_mail_wpnonce" value="<?php echo wp_create_nonce( 'send-request-quote' ) ?>">
                    <input class="button raq-send-request" type="submit" value="<?php _e( 'Send Your Request', 'yith-woocommerce-request-a-quote' ) ?>">
                </p>
            </div>

        </div><!-- End of row -->
    </form>
</div>