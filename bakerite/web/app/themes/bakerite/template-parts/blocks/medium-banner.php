	<!-- Medium Banner Start -->
	<div class="container-fluid medium-banner-container px-0">
		<div class="row">
			<div class="col-md-6 medium-banner-left pr-md-0">
				<img class="img-fluid" src="<?php echo  the_field('medium_banner_image'); ?>" alt="Bakerite banner image">
			</div>
			<div class="col-md-6 medium-banner-right">
				<div class="row">
					<div class="col-md-12 col-lg-12 medium-banner-text">
						<?php echo the_field('medium_banner_text'); ?>
					</div>
				</div>
			</div>
		</div>
		<img class="img-fluid medium-banner-icon" src="<?php echo the_field('medium_banner_icon'); ?>" alt="Baketire">
	</div>
	<!-- Medium Banner End -->