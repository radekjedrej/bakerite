	<div class="section-assurance-steps">
		<div class="container assurance-steps-container">
			<!-- 1st Box -->
			<div class="row pb-0 assurance-steps-row">
				<div class="col-lg-7 pr-lg-0 assurance-relative-box">
					<div class="navigation-horizontal-line"></div>
					<div class="assurance-steps-bg">
						<div class="assurance-steps-inner">
							<?php echo the_field('quality_assurance_text_box_1'); ?>
						</div>
						<div class="assurance-steps-image">
							<img class="img-anim-1" src="<?php echo the_field('quality_assurance_image_box_1'); ?>" alt="Bakerite Product Image">
						</div>
					</div>
				</div>
				<div class="d-none d-lg-block col-lg-5 pl-lg-0 assurance-steps-drop">
					<img class="" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/a-right-drop.svg" alt="Next Step">
				</div>
			</div>
			<!-- 2nd Box -->
			<div class="row pb-0 assurance-steps-row-2">
				<div class="d-none d-lg-block d-lg-block col-lg-5 pr-lg-0 assurance-steps-drop-2">
					<img class="" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/a-left-drop.svg" alt="Next Step">
				</div>
				<div class="col-lg-7 pl-lg-0">
					<div class="navigation-horizontal-line"></div>
					<div class="assurance-steps-bg-2">
						<div class="assurance-steps-inner-2">
							<?php echo the_field('quality_assurance_text_box_2'); ?>
						</div>
						<div class="assurance-steps-image-2">
							<img class="img-anim-2" src="<?php echo the_field('quality_assurance_image_box_2'); ?>" alt="Bakerite Product Image">
						</div>
					</div>
				</div>
			</div>
			<!-- 3rd Box -->
			<div class="row pb-0 assurance-steps-row">
				<div class="col-lg-7 pr-lg-0 assurance-relative-box">
					<div class="navigation-horizontal-line"></div>
					<div class="assurance-steps-bg">
						<div class="assurance-steps-inner">
							<?php echo the_field('quality_assurance_text_box_3'); ?>
						</div>
						<div class="assurance-steps-image">
							<img class="img-anim-3" src="<?php echo the_field('quality_assurance_image_box_3'); ?>" alt="Bakerite Product Image">
						</div>
					</div>
				</div>
				<div class="d-none d-lg-block col-lg-5 pl-lg-0 assurance-steps-drop">
					<img class="" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/a-right-drop.svg" alt="Next Step">
				</div>
			</div>
			<!-- 4th Box -->
			<div class="row pb-0 assurance-steps-row-2">
				<div class="d-none d-lg-block col-lg-5 pr-lg-0 assurance-steps-drop-2">
					<img class="" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/icons/a-left-drop.svg" alt="Next Step">
				</div>
				<div class="col-lg-7 pl-lg-0">
					<div class="navigation-horizontal-line"></div>
					<div class="assurance-steps-bg-2">
						<div class="assurance-steps-inner-2">
							<?php echo the_field('quality_assurance_text_box_4'); ?>
						</div>
						<div class="assurance-steps-image-2">
							<img class="img-anim-4" src="<?php echo the_field('quality_assurance_image_box_4'); ?>" alt="Bakerite Product Image">
						</div>
					</div>
				</div>
			</div>
			<!-- 5th Box -->
			<div class="row pb-0 assurance-steps-row">
				<div class="col-lg-7 pr-lg-0 assurance-relative-box">
					<div class="navigation-horizontal-line"></div>
					<div class="assurance-steps-bg">
						<div class="assurance-steps-inner">
							<?php echo the_field('quality_assurance_text_box_5'); ?>
						</div>
						<div class="assurance-steps-image">
							<img class="img-anim-5" src="<?php echo the_field('quality_assurance_image_box_5'); ?>" alt="Bakerite Product Image">
						</div>
					</div>
				</div>
				<div class="col-lg-5 pl-lg-0 assurance-steps-drop">
				</div>
			</div>

		</div>
	</div>