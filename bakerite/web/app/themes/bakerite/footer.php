<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bakerite
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="navigation-horizontal-line"></div>
			<div class="footer-wrapper">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 pr-lg-1">
							<div class="footer-title footer-image-wrapper">
								<img class="img-fluid" src="<?php echo home_url(); ?>/app/themes/bakerite/assets/img/logo-footer.png" alt="Logo Bakerite">
							</div>
							<?php
								wp_nav_menu( array(
									'menu' => 'Footer Menu First',
									'menu_class' => 'navbar-nav nav-second'
								) );
							?>
						</div>
						<div class="col-md-6 col-lg-3 pl-lg-2">
							<div class="footer-title">USEFUL LINKS</div>
							<?php
								wp_nav_menu( array(
									'menu' => 'Footer Menu Second',
									'menu_class' => 'navbar-nav nav-third'
								) );
							?>
						</div>
						<div class="col-md-6 col-lg-3 pr-0">
							<div class="footer-title">CONTACT DETAILS</div>
								<ul>
									<li><p>The TradeLink International Group Ltd,</p></li>
									<li><p>Melton House, Melton Business Park</p></li>
									<li><p>Melton,</p></li>
									<li><p>HU14 3HJ</p></li>
								</ul>
						</div>
					</div>
				</div>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQfDiaoBKieDd3Qa8mjeTb1xuaTJPDUwg&callback=initMap" async defer></script>


</body>
</html>
