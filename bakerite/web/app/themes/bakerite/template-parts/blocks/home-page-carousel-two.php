<?php
$artworks = apply_filters('home_page_carousel_two', get_field('home_page_carousel_two'));
if(empty($artworks)) return;
?>

<section class="home-page-carousel-two">
  <div class="container">
    <div class="row px-3">
        <div class="owl-carousel-home-page-two">

          <?php foreach($artworks as $artwork): ?>

            <div class="slide">
              <div class="main-carousel-wrapper">
                <img class="img-fluid" src="<?php echo $artwork['image']; ?>" alt="">
                <div class="main-carousel-text-wrapper">
                  <?php echo $artwork['text']; ?>
                </div>
              </div>             
            </div>

          <?php endforeach; ?>

        </div>
    </div>
  </div>
</section>