<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php get_template_part( 'template-parts/blocks/main-banner'); ?>
			<?php get_template_part( 'template-parts/blocks/three-boxes'); ?>
			<?php get_template_part( 'template-parts/blocks/home-page-text'); ?>
			<?php get_template_part( 'template-parts/blocks/categories'); ?>
			<?php get_template_part( 'template-parts/blocks/home-page-carousel-two'); ?>
			<?php get_template_part( 'template-parts/blocks/contact'); ?>
			


		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
