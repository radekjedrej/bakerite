	<!-- Contact Form Start -->
  <div class="entry-content contact-main-section">
    <div class="container container-contact contact">
      <div class="contact-heading text-center">
        <h1>Get in touch with BakeRite today,  just fill in our enquiry form below</h1>
      </div>
			<?php echo the_field('contact_us'); ?>
		</div>
	</div>	
	<!-- Contact Form End -->