<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bakerite
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="side-header">
		<div class="header-number-div">
			<div class="container header-number-container text-uppercase text-sm-center text-md-right titles">
				CALL US FREE ON <span><a href="tel:<?php the_field('number', 'option'); ?>"><?php the_field('number', 'option'); ?></a></span> OR EMAIL <span><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></span>
			</div>
		</div>

		<div class="container tag-search-container">
			<div class="row">
				<div class="col-md-3">
					<div class="header-logo-wrapper">
						<a href="<?php echo home_url(); ?>"><img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/logo.png" alt="Bakerite"></a> 
					</div>
				</div>
				<div class="d-none d-lg-flex tag-search-wrapper col-md-9 ">
					<?php
						if ( is_active_sidebar( 'shop_sidebar_1' ) ) : ?>
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'shop_sidebar_1' ); ?>
							</div><!-- #primary-sidebar -->
					<?php endif; ?>			
					<div class="choose-tag">
						<div class="choose-tag-button">
							SELECT YOUR SEARCH
						</div>
					</div>		
				</div>
			</div>
		</div>


	<div class="section" id="main-menu">
		<div class="main-navigation d-block">
			<div class="navigation-horizontal-line"></div>
			<div class="main-navigation-wrapper">
				<div class="container">
					<nav id="site-navigation" class="main-navigation-inner navbar navbar-expand-lg px-0">

						<button type="button" class="tcon tcon-menu--xbutterfly navbar-toggler mr-auto" aria-label="toggle menu" data-toggle="collapse" data-target="#navbarNav">
                <span class="tcon-menu__lines" aria-hidden="true"></span>
                <span class="tcon-visuallyhidden">toggle menu</span>
            </button>					
								
						<div class="collapse navbar-collapse" id="navbarNav">		
						<?php
							wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
							'menu_class' => 'navbar-nav'
						) );
						?>
						</div>

					</nav><!-- #site-navigation -->		
				</div>			
			</div>
		</div>
	</div>

						
	</header><!-- #masthead -->

	<div class="clearfix"></div>

	<div id="content" class="site-content">
