<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bakerite
 */

get_header();
?>
	


<section class="contact-us">

	<?php get_template_part( 'template-parts/blocks/medium-banner'); ?>
	<?php get_template_part( 'template-parts/blocks/contact-options'); ?>
	<?php get_template_part( 'template-parts/blocks/contact-large'); ?>


	<!-- Map Start -->
	<div class="map-section">
		<div class="map-wrapper">
			<div id="map" class="map"></div>
		</div>
		<div class="map-text-wrapper">
			<div class="container map-text-container">
				<div class="row">
					<div class=" map-box-col">
						<div class="map-box-title">
							<img src="<?php home_url(); ?>/app/themes/bakerite/assets/img/icons/map.svg" alt="Map icon">
							<h1>BakeRite Address</h1>
						</div>
						<div class="map-box-text">
							<p>The BakeRite Company</p>
							<p>The TradeLink International Group Ltd</p>
							<p>United Kingdom, Melton House</p>
							<p>Melton Business Park, Melton</p>
							<p>HU14 3HJ</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Map End -->

</section>



<?php

get_footer();
